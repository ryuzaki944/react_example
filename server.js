const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')


const items = require('./routes/api/items')

const app = express();

// Body-Parser Middleware
app.use(bodyParser.json())

// DB CONNECT
mongoose.connect('mongodb://localhost:27017/reactdb', {useNewUrlParser: true, useUnifiedTopology: true})
const db = mongoose.connection;

db.on('error',(err) => {
    console.log(err)
})

db.once('open', () => {
    console.log('DB CONNECTED...')
})

// USE ROUTES
app.use('/api/items', items)

const PORT = process.env.PORT || 5000

app.listen(PORT, () => console.log(`Server on ${PORT}`))

